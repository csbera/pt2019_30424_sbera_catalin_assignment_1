
package model;

public class Monom {
  
    private float coeficient;
    private float power;
    
    
    public Monom(float coeficient, float power) {
        this.coeficient = coeficient;
        this.power = power;
  
    }
    
    public float getPower() {
      return this.power;
    }
    public float getCoeficient() {
      return this.coeficient;
    }

    public boolean equalPower(Monom monom) {
      return this.power == monom.getPower();
    }
    
    public void add(Monom monom) {
      this.coeficient += monom.getCoeficient();     
    }
    
    public void derivate() {
      this.coeficient *= power;
      if(this.power > 0) {
        this.power = this.power - 1;
      }
    }
    
    public void integrate() {
      this.power = this.power + 1;
      this.coeficient /= power;
    }
    
    public void multiply(Monom m) {
      this.coeficient *= m.getCoeficient();
      if(m.getPower() == 0 && m.getCoeficient() == 0)
        this.power = 0;
      else
        this.power += m.getPower();
    }
    
    public void divideBy(Monom m) {
      if(m.getCoeficient() != 0) {
        this.coeficient /= m.getCoeficient();
        this.power -= m.getPower();
      }
    }
    
    public static Monom multiply(Monom m1, Monom m2) {
      Monom rez = new Monom(1,0);
      rez.multiply(m1);
      rez.multiply(m2);
      return rez;
    }
    
    public Monom getInverse() {
      Monom aux = new Monom(-coeficient,power);
      return aux; 
    }
    
    public static Monom getCopy(Monom m) {
      return new Monom(m.getCoeficient(),m.getPower());
    }
    
    @Override
    public String toString() {
      String rez = "";
      if((int)coeficient != 1) {
        if((int)coeficient > 0) {
          rez += '+';
        }
        rez += (int)coeficient;
      }else if ((int)power == 0)
        rez += "+1";
      else
        rez += '+';
      if((int)power != 0)
        if((int)power != 1)
          rez+= "x^" + (int)power;
        else
         rez+= "x";
      return rez;
    }

}
