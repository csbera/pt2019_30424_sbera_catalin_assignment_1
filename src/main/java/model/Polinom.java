package model;

import java.util.ArrayList;
import java.util.Collections;


public class Polinom {
    private ArrayList<Monom> monoms;
    
    
    public Polinom() {
      this.monoms = new ArrayList<Monom>();
    }
    
    private ArrayList<Monom> getMonoms() {
      return this.monoms;
    }

    
    public void add(Polinom polinom) {
      ArrayList<Monom> monoms = polinom.getMonoms();
      for(Monom m : monoms) {
        this.addToPolinom(m);
      }
      if(this.monoms.size() == 0)
        this.addToPolinom(new Monom(0,0));
    }
    
    public void subtract(Polinom polinom) {
      ArrayList<Monom> monoms = polinom.getMonoms();
      for(Monom m : monoms) {
        this.addToPolinom(m.getInverse());
      }
      if(this.monoms.size() == 0)
        this.addToPolinom(new Monom(0,0));
    }
    
    public void integrate() {
      ArrayList<Monom> monomsToRemove = new ArrayList<Monom>();
      for(Monom m : monoms) {
        m.integrate();
        if((int)m.getCoeficient() == 0 || (int)m.getCoeficient() < m.getCoeficient()) {
          monomsToRemove.add(m);
        }
      }
      for(Monom m : monomsToRemove) {
        monoms.remove(m);
      }
      if(this.monoms.size() == 0)
        this.addToPolinom(new Monom(0,0));
    }
    
    public void derivate() {
      ArrayList<Monom> monomsToRemove = new ArrayList<Monom>();
      for(Monom m : monoms) {
        m.derivate();
        if(m.getPower() < 0) {
          monomsToRemove.add(m);
        }
      }
      for(Monom m : monomsToRemove) {
        monoms.remove(m);
      }
      if(this.monoms.size() == 0)
        this.addToPolinom(new Monom(0,0));
    }
    
    public float getHighestDegree(){
      return this.monoms.get(0).getPower();
    }
    
    public boolean isNull() {
      return this.monoms.size() == 1 && this.monoms.get(0).toString().equals("0");
    }
    
    public Polinom multiply(Polinom polinom) {
      Polinom rez = new Polinom();
      ArrayList<Monom> monomsToRemove = new ArrayList<Monom>();
      ArrayList<Monom> monoms = polinom.getMonoms();
      for(Monom m1 : this.monoms) {
        for(Monom m2 : monoms) {
          rez.addToPolinom(Monom.multiply(m1,m2));
        }
      }
      for(Monom m : rez.monoms) {
        if(m.getCoeficient() == 0)
          monomsToRemove.add(m);
      }
      for(Monom m : monomsToRemove) {
        rez.monoms.remove(m);
      }
      if(this.monoms.size() == 0)
        this.addToPolinom(new Monom(0,0));
      return rez;
    }
    
    public void multiplyBy(Monom monom) {
      for(Monom m : this.monoms) {
        m.multiply(monom);
      }
      if(this.monoms.size() == 0)
        this.addToPolinom(new Monom(0,0));
    }
    
    public Polinom divide(Polinom polinom) {
      Polinom rez = new Polinom();
      rez.addToPolinom(new Monom(0,0));
      if(this.getHighestDegree() == 0) {
        ArrayList<Monom> monomsToRemove = new ArrayList<Monom>();
        rez = Polinom.getCopy(polinom);
        Monom divident = this.monoms.get(0);
        for(Monom m : rez.monoms) {
          m.divideBy(divident);
          if((int)m.getCoeficient() == 0)
            monomsToRemove.add(m);
        }
        for(Monom m : monomsToRemove) {
          rez.monoms.remove(m);
        }
        rez.addToPolinom(new Monom(0,0));
        return rez;
      }
      if(polinom.getHighestDegree() >= this.getHighestDegree()) {
        Polinom auxThis = Polinom.getCopy(this);
        Polinom auxPoli = Polinom.getCopy(polinom);
        while(auxPoli.getMonoms().size() > 0 && auxPoli.getHighestDegree() >= this.getHighestDegree()) {
          Monom firstElement = Monom.getCopy(auxPoli.getMonoms().get(0));
          firstElement.divideBy(this.getMonoms().get(0));
          auxThis.multiplyBy(firstElement);
          rez.addToPolinom(firstElement);
          auxPoli.subtract(auxThis);
          auxThis = Polinom.getCopy(this);
        }
      }
      return rez;
    }
    
    public void addToPolinom(Monom monom) {
      for(Monom m : monoms) {
        if (m.equalPower(monom)) {
          m.add(monom);
          if(m.getCoeficient() == 0) {
            monoms.remove(m);
          }
          return;
        }
      }
      this.monoms.add(monom);
      Collections.sort(this.monoms, new SortByPower());
    }
    
    public static Polinom getCopy(Polinom p) {
      Polinom rez = new Polinom();
      ArrayList<Monom> monoms = p.getMonoms();
      for(Monom m : monoms) {
        rez.addToPolinom(Monom.getCopy(m));
      }
      return rez;
    }

    @Override
    public String toString() {
      boolean isZero = true;
      if(monoms.size() > 1) {
        isZero = false;
      }
      String rez = "";
      for(Monom m : monoms) {
        if(m.getCoeficient() == m.getPower() && m.getCoeficient() == 0 && !isZero) continue;
        rez += m.toString();
      }
      if(rez.length() != 0 && rez.charAt(0) == '+')
       return rez.substring(1);
      return rez;
    }
    
    
}
