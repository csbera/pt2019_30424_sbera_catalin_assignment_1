package model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {

  public static Polinom convertToPolinom(String input) {
    input = input.replaceAll("\\s+","");
    Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
    Matcher matcher = pattern.matcher(input);
    Polinom polinom = new Polinom();
    if(input.length() == 0) {
      polinom.addToPolinom(new Monom(0,0));
      return polinom;
    }
    try{
      while (matcher.find()) {
        polinom.addToPolinom(Reader.makeMonom(matcher.group(1)));
       }
      return polinom;
    }catch(NumberFormatException e1) {
      System.out.println("Invalid input nfe");
      return null;
    }catch(StringIndexOutOfBoundsException e2) {
      System.out.println("Invalid input soob");
      return null;
    }catch(ArrayIndexOutOfBoundsException e2) {
      System.out.println("Invalid input aoob");
      return null;
    }
  }
  
  private static Monom makeMonom(String string) {
    Monom auxMonom;
    if(string.startsWith("+x") || string.startsWith("-x") || string.startsWith("x")) {
      if(string.length() <= 2) {
        if(string.charAt(0) == '-')
          return new Monom(-1,1);
        else
          return new Monom(1,1);
      }
      int start = 2;
      if(string.charAt(0) != 'x')
        start++;
      auxMonom = new Monom(1,Integer.parseInt(string.substring(start,string.length())));
    }else if (string.endsWith("x")) {
      auxMonom = new Monom(Integer.parseInt(string.substring(0,string.length() - 1)),1);
      
    }else if(string.indexOf('x') == -1){
      auxMonom = new Monom(Integer.parseInt(string),0);
    }else {
      String[] aux;
      aux = string.split("x");
      auxMonom = new Monom(Integer.parseInt(aux[0]),Integer.parseInt(aux[1].substring(1, aux[1].length())));
    }
    return auxMonom;
  }
}
