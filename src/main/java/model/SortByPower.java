package model;

import java.util.Comparator;

public class SortByPower implements Comparator<Monom> 
{ 
    // Used for sorting in ascending order of power
    public int compare(Monom a, Monom b) 
    { 
        return (int) (b.getPower() - a.getPower()); 
    } 
}