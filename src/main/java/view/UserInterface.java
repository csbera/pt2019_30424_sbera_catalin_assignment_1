package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.*;

import javax.swing.*;

public class UserInterface extends Frame{
  
  public UserInterface () {  

     JFrame f= new JFrame("PolinomApp");  
       
     JPanel panelText=new JPanel();
     panelText.setLayout(new GridLayout(2, 2));
     JLabel errorTextArea=new JLabel("");  
     JTextField polinomTextField=new JTextField("Input a polinom here");      
     JButton addToListButton=new JButton("Add polinom to list");
     panelText.add(polinomTextField);
     panelText.add(addToListButton);
     panelText.add(errorTextArea); 
     f.add(panelText,BorderLayout.NORTH);
     
     //String defaultPolinoms[]= { "0","1","x","x+1","x-1","x^2+2x+1"};
     
     DefaultListModel<String> dlm = new DefaultListModel<String>();
     dlm.addElement("0                ");
     
     //JPanel panelPolinomOperations = new JPanel();
     panelText.setLayout(new GridLayout(0,3));
     JList<String> polinomSelectionList1 = new JList<>(dlm);
     JScrollPane polinomSelectionList1ScrollPane = new JScrollPane(polinomSelectionList1);
     JPanel panelButtons = new JPanel();
     panelButtons.setLayout(new GridLayout(3, 2));
     JButton addButton = new JButton("Add");
     panelButtons.add(addButton);
     JButton subtractButton = new JButton("Subtract");
     panelButtons.add(subtractButton);
     JButton multiplyButton = new JButton("Multiply");
     panelButtons.add(multiplyButton);
     JButton divideButton = new JButton("Divide");
     panelButtons.add(divideButton);
     JButton integrateButton = new JButton("Integrate");
     panelButtons.add(integrateButton);
     JButton derivateButton = new JButton("Derivate");
     panelButtons.add(derivateButton);
     JList<String> polinomSelectionList2 = new JList<>(dlm);
     JScrollPane polinomSelectionList2ScrollPane = new JScrollPane(polinomSelectionList2);
     JPanel panelResult = new JPanel();
     JLabel resultLabel=new JLabel("Result:");  
     JTextArea resultTextArea=new JTextArea("");  
     panelResult.add(resultLabel);
     panelResult.add(resultTextArea);
     f.add(polinomSelectionList1ScrollPane,BorderLayout.WEST);
     f.add(panelButtons,BorderLayout.CENTER);
     f.add(polinomSelectionList2ScrollPane,BorderLayout.EAST);
     f.add(panelResult,BorderLayout.SOUTH);
     
     addToListButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) { 
         polinomTextField.getText();
         Polinom polinom = Reader.convertToPolinom(polinomTextField.getText());
         if(polinom == null) {
           errorTextArea.setText("Invalid Input!");
         }
         else {
           errorTextArea.setText("Polinom added successfully");
           dlm.addElement(polinom.toString());
       } 
     }} );
     
     subtractButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) { 
         Polinom poli1,poli2;
         
         if(polinomSelectionList1.getSelectedValue() != null) {
           poli1 = Reader.convertToPolinom(polinomSelectionList1.getSelectedValue());
         }
         else {
           poli1 = Reader.convertToPolinom("0");
         }
         if(polinomSelectionList2.getSelectedValue() != null) {
           poli2 = Reader.convertToPolinom(polinomSelectionList2.getSelectedValue());
         }
         else {
           poli2 = Reader.convertToPolinom("0");
         }
         poli1.subtract(poli2);
         resultTextArea.setText(poli1.toString());
     }} );
     
     addButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) {
         Polinom poli1,poli2;
         if(polinomSelectionList1.getSelectedValue() != null) {
           poli1 = Reader.convertToPolinom(polinomSelectionList1.getSelectedValue());
         }
         else {
           poli1 = Reader.convertToPolinom("0");
         }
         if(polinomSelectionList2.getSelectedValue() != null) {
           poli2 = Reader.convertToPolinom(polinomSelectionList2.getSelectedValue());
         }
         else
           poli2 = Reader.convertToPolinom("0");
         poli1.add(poli2);
         resultTextArea.setText(poli1.toString());
     }} );
     
     multiplyButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) { 
         Polinom poli1,poli2;
         
         if(polinomSelectionList1.getSelectedValue() != null) {
           poli1 = Reader.convertToPolinom(polinomSelectionList1.getSelectedValue());
         }
         else {
           poli1 = Reader.convertToPolinom("0");
         }
         if(polinomSelectionList2.getSelectedValue() != null) {
           poli2 = Reader.convertToPolinom(polinomSelectionList2.getSelectedValue());
         }
         else {
           poli2 = Reader.convertToPolinom("0");
         }
         poli1 = poli1.multiply(poli2);
         resultTextArea.setText(poli1.toString());
     }} );
     
     divideButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) { 
         Polinom poli1,poli2;
         
         if(polinomSelectionList1.getSelectedValue() != null) {
           poli1 = Reader.convertToPolinom(polinomSelectionList1.getSelectedValue());
         }
         else {
           poli1 = Reader.convertToPolinom("0");
         }
         if(polinomSelectionList2.getSelectedValue() != null) {
           poli2 = Reader.convertToPolinom(polinomSelectionList2.getSelectedValue());
         }
         else {
           poli2 = Reader.convertToPolinom("0");
         }
         if(poli2.isNull()) {
           resultTextArea.setText("Cannot divide by 0");
         }
         else {
           poli1 = poli2.divide(poli1);
           resultTextArea.setText(poli1.toString());
         }
     }} );
     
     integrateButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) { 
         Polinom poli1;
         if(polinomSelectionList1.getSelectedValue() != null) {
           poli1 = Reader.convertToPolinom(polinomSelectionList1.getSelectedValue());
         }
         else
           poli1 = Reader.convertToPolinom("0");
         poli1.integrate();
         resultTextArea.setText(poli1.toString());
         
     }} );
     
     derivateButton.addActionListener(new ActionListener() { 
       public void actionPerformed(ActionEvent e) { 
         Polinom poli1;
         if(polinomSelectionList1.getSelectedValue() != null) {
           poli1 = Reader.convertToPolinom(polinomSelectionList1.getSelectedValue());
         }
         else
           poli1 = Reader.convertToPolinom("0");
         poli1.derivate();
         resultTextArea.setText(poli1.toString());
     }} );
     
     
     f.setSize(600,300);       
     f.setVisible(true);
     
 }  
   
 public static void main(String args[]){  
   UserInterface b = new UserInterface();  
 }  
}
