package model;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Monom;

public class MonomTest {

  
  @Test
  public void testMonom_ToString_WhenMonomIsNull() {
    Monom m = new Monom(0,0);
    assertTrue(m.toString().equals("0"));
  }
  
  @Test
  public void testMonom_ToString_WhenMonomIsNotNull() {
    Monom m1 = new Monom(1,1);
    Monom m2 = new Monom(2,2);
    assertTrue(m1.toString().equals("+x"));
    assertTrue(m2.toString().equals("+2x^2"));
  }
  
  @Test
  public void testMonom_Add() {
    Monom m1 = new Monom(1,1);
    Monom m2 = new Monom(2,2);
    Monom m3 = new Monom(0,0);
    m1.add(m2);
    assertTrue(m1.toString().equals("+3x"));
    m1.add(m3);
    assertTrue(m1.toString().equals("+3x"));
  }
  
  @Test
  public void testMonom_Derivate() {
    Monom m0 = new Monom(0,0);
    Monom m1 = new Monom(1,1);
    Monom m2 = new Monom(2,2);
    m0.derivate();
    m1.derivate();
    m2.derivate();
    assertTrue(m0.toString().equals("0"));
    assertTrue(m1.toString().equals("+1"));
    assertTrue(m2.toString().equals("+4x"));
  }
  
  @Test
  public void testMonom_Multiply() {
    Monom m0 = new Monom(0,0);
    Monom m1 = new Monom(1,1);
    Monom m2 = new Monom(2,2);
    m0.multiply(m1);
    m1.multiply(m2);
    m2.multiply(m0);
    assertTrue(m0.toString().equals("0x"));
    assertTrue(m1.toString().equals("+2x^3"));
    assertTrue(m2.toString().equals("0x^3"));
  }

}
